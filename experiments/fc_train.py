import numpy as np
import _pickle as pickle
from math import sqrt
from pybrain.datasets.supervised import SupervisedDataSet as SDS
from pybrain.structure.modules.sigmoidlayer import SigmoidLayer
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules import SoftmaxLayer
from pybrain.structure.modules.linearlayer import LinearLayer
from pybrain.structure.modules.tanhlayer import TanhLayer
import random as random
import timeit
from experiments import fc_helper


def main():
    fc_helper.run()
    x_train = np.load(fc_helper.data_path['fc_x_train'])
    y_train = np.load(fc_helper.data_path['fc_y_train'])

    output_model_file = fc_helper.model_path['model_file']

    input_size = fc_helper.config['input_size']
    target_size = fc_helper.config['numlabels']
    hidden_size = fc_helper.config['hidden_size']
    epochs = fc_helper.config['epochs']
    learningrate = fc_helper.config['learningrate']

    #
    np.random.seed(1)

    ds = SDS(input_size, target_size)
    ds.setField('input', x_train)
    ds.setField('target', y_train)

    # init and train

    net = buildNetwork(input_size, hidden_size, target_size, outclass=SoftmaxLayer, bias=True)

    trainer = BackpropTrainer(net, ds, learningrate)

    print("training for {} epochs...".format(epochs))

    for i in range(epochs):
        mse = trainer.train()
        rmse = sqrt(mse)
        print("training RMSE, epoch {}: {}".format(i + 1, rmse))

    pickle.dump(net, open(output_model_file, 'wb'))


if __name__ == '__main__':
    main()
