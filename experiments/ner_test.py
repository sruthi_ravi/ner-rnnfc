import numpy as np

from experiments import helper


def predict(x_test, y_test):
    # read the parameters from the numpy file and do the forward propagation
    u = np.load(helper.parameter_file['u'])
    v = np.load(helper.parameter_file['v'])
    w = np.load(helper.parameter_file['w'])

    rnn = helper.init_model(u=u, v=v, w=w)
    accurate = 0
    t_pred = 0
    t_actu = 0

    for i in range(len(x_test)):
        output = rnn.predict(x_test[i])
        p, r, correct = get_metrics(np.array(y_test[i]), output)
        t_pred += p
        t_actu += r
        accurate += correct
    precision = accurate / t_pred
    recall = accurate / t_actu
    f1 = 2 * precision * recall / (precision + recall)

    print(
        'F1 score  {}'.format(f1 * 100))
    print(
        'Precision score   {}'.format(precision * 100))
    print(
        'Recall score  {}'.format(recall * 100))


def get_metrics(y_actu, y_pred):
    correct = 0

    c_pred = np.count_nonzero(y_pred)
    c_actu = np.count_nonzero(y_actu)
    for x, y in np.nditer([y_actu, y_pred]):
        if x != 0 and y != 0 and x == y:
            correct += 1
    return c_pred, c_actu, correct



def main():
    x_test = np.load(helper.data_path['x_test'])
    y_test = np.load(helper.data_path['y_test'])
    x_train = np.load(helper.data_path['x_train'])
    y_train = np.load(helper.data_path['y_train'])
    print('Testing....')
    predict(x_test, y_test)



if __name__ == '__main__':
    main()
