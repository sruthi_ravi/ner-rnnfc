import numpy as np
from experiments import helper

parameters = {
    'u': '../parameters/u.npy',
    'v': '../parameters/v.npy',
    'w': '../parameters/w.npy',
    'u_rev': '../parameters/u_rev.npy',
    'v_rev': '../parameters/v_rev.npy',
    'w_rev': '../parameters/w_rev.npy'
}

data_path = {
    'x_train': '../data/gen/trainX_notypes.npy',
    'y_train': '../data/gen/trainY_notypes.npy',
    'x_test': '../data/gen/testX_notypes.npy',
    'y_test': '../data/gen/testY_notypes.npy',
    'rev_x_train': '../data/reversernn/rev_trainX_notypes.npy',
    'rev_y_train': '../data/reversernn/rev_trainY_notypes.npy',
    'rev_x_test': '../data/reversernn/rev_testX_notypes.npy',
    'rev_y_test': '../data/reversernn/rev_testY_notypes.npy',
    'fc_x_train': '../data/fc/fc_x_train.npy',
    'fc_x_test': '../data/fc/fc_x_test.npy',
    'fc_y_train': '../data/fc/fc_y_train.npy',
    'fc_y_test': '../data/fc/fc_y_test.npy',
    'y_train_flat': '../data/fc/y_train_flat.npy',
    'y_test_flat': '../data/fc/y_test_flat.npy'

}

config = {

    'input_size': 6,
    'target_size': 3,
    'hidden_size': 50,
    'epochs': 100,
    'learningrate': 0.005,
    'numlabels': 3
}

model_path = {
    'model_file':'../parameters/fc_train_model.pkl',
    'output_file': '../output/fc_predictions.txt'
}
def get_prob_values(data, u=None, v=None, w=None):
    # read the parameters from the numpy file and do the forward propagation

    rnn = helper.init_model(u=u, v=v, w=w)
    total_prob = []
    for i in range(len(data)):
        output = rnn.probability(data[i])
        total_prob.append(output)
    return np.array(total_prob)


def concat_data(data1, data2):
    # reverse the data for concatenation
    rev_data2 = []
    for i in range(len(data2)):
        rev_data2.append(data2[i][::-1])
    rev_data2 = np.array(rev_data2)
    print('done')

    concat = []
    for i in range(len(data1)):
        concat.append(np.concatenate((data1[i], rev_data2[i]), axis=1))
    return np.array(concat)


def flatten(data, size=6):
    flat = np.reshape(data[0], (len(data[0]), size))
    for i in range(1, len(data)):
        temp = np.reshape(data[i], (len(data[i]), size))
        flat = np.concatenate((flat, temp), axis=0)
    return flat


def one_hot_encoded(data, numlabels=3):
    y_train = data.reshape((len(data)))
    rows = y_train.shape[0]
    Y2 = np.zeros((rows, numlabels))
    for i in range(0, rows):
        Y2[i, y_train[i]] = 1
    return Y2

    # y_train = np.load('../data/gen/trainY_types.npy')
    # rows = len(y_train)
    # numLabels = 7
    # y_train_enc = []
    # for i in range(0, rows):
    #     Y2 = np.zeros((len(y_train[i]), numLabels))
    #     for j in range(0, len(y_train[i])):
    #         Y2[j, y_train[i][j]] = 1
    #     y_train_enc.append(Y2)
    # np.save('../data/gen/sampleY.npy', np.array(y_train_enc))


# convertToOneOfMany()

def find_max_len():
    data = np.load('../data/gen/devX_notypes.npy')
    max_len = -1
    for i in range(len(data)):
        lent = len(data[i])
        if lent > max_len:
            max_len = lent
    print(max_len)


def run():
    # load data
    u = np.load(parameters['u'])
    v = np.load(parameters['v'])
    w = np.load(parameters['w'])
    u_rev = np.load(parameters['u_rev'])
    v_rev = np.load(parameters['v_rev'])
    w_rev = np.load(parameters['w_rev'])
    x_train = np.load(data_path['x_train'])
    y_train = np.load(data_path['y_train'])
    x_test = np.load(data_path['x_test'])
    y_test = np.load(data_path['y_test'])
    rev_x_train = np.load(data_path['rev_x_train'])
    rev_x_test = np.load(data_path['rev_x_test'])

    train_prob = get_prob_values(x_train, u=u, v=v, w=w)
    test_prob = get_prob_values(x_test, u=u, v=v, w=w)
    rev_train_prob = get_prob_values(rev_x_train, u=u_rev, v=v_rev, w=w_rev)
    rev_test_prob = get_prob_values(rev_x_test, u=u_rev, v=v_rev, w=w_rev)

    concat_train = concat_data(train_prob, rev_train_prob)
    concat_test = concat_data(test_prob, rev_test_prob)
    # size depends on the size of the concatenation = no of output classes * 2

    np.save(data_path['fc_x_train'], flatten(concat_train, size=config['input_size']))
    np.save(data_path['fc_x_test'], flatten(concat_test, size=config['input_size']))

    y_train_flat = flatten(y_train, size=1)
    np.save(data_path['y_train_flat'], y_train_flat)
    y_test_flat = flatten(y_test, size=1)
    np.save(data_path['y_test_flat'], y_test_flat)

    np.save(data_path['fc_y_train'], one_hot_encoded(y_train_flat, numlabels=config['numlabels']))
    np.save(data_path['fc_y_test'], one_hot_encoded(y_test_flat, numlabels=config['numlabels']))


if __name__ == '__main__':
    run()
