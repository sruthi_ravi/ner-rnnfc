import numpy as np
import _pickle as pickle
from experiments import fc_helper

from pybrain.datasets.supervised import SupervisedDataSet as SDS

from experiments import ner_test

model_file = fc_helper.model_path['model_file']
output_predictions_file = fc_helper.model_path['output_file']
actual= np.load(fc_helper.data_path['y_test_flat'])

# load model

net = pickle.load(open(model_file, 'rb'))

# load data

x_test = np.load(fc_helper.data_path['fc_x_test'])
y_test = np.load(fc_helper.data_path['fc_y_test'])

input_size = fc_helper.config['input_size']
target_size = fc_helper.config['numlabels']

assert (net.indim == input_size)
assert (net.outdim == target_size)

# prepare dataset

ds = SDS(input_size, target_size)
ds.setField('input', x_test)
ds.setField('target', y_test)

# predict

p = net.activateOnDataset(ds)
#

np.savetxt(output_predictions_file, p, fmt='%.6f')
predicted = []
with open(output_predictions_file) as read:
    for line in read:
        line = line.strip()
        val = line.split()
        predicted.append(val.index(max(val)))
read.close()

actual = actual.reshape(len(actual))
c_pred, c_actu, correct = ner_test.get_metrics(actual, np.array(predicted))

precision = correct / c_pred
recall = correct / c_actu
f1 = 2 * precision * recall / (precision + recall)
print(
    'F1 score : {}'.format(f1 * 100))
print(
    'Precision score : {}'.format(precision * 100))
print(
    'Recall score : {}'.format(recall * 100))

# precision recall
