from sklearn import datasets, linear_model
import numpy as np
from experiments import ner_test

x_train = np.load('../data/gen/fc_trainX_flat.npy')
y_train = np.load('../data/gen/fc_trainY_flat.npy')
y_train = y_train.reshape(len(y_train))
x_test = np.load('../data/gen/fc_testX_flat.npy')
y_test = np.load('../data/gen/fc_testY_flat.npy')
y_test = y_test.reshape(len(y_test))
# digits = datasets.load_digits()
# X_digits = digits.data
# y_digits = digits.target
#
# n_samples = len(X_digits)
#
# X_train = X_digits[:int(.9 * n_samples)]
# y_train = y_digits[:int(.9 * n_samples)]
# X_test = X_digits[int(.9 * n_samples):]
# y_test = y_digits[int(.9 * n_samples):]

logistic = linear_model.LogisticRegression(C=0.001)
logistic.fit(x_train, y_train)
predicted = logistic.predict(x_test)
c_pred, c_actu, correct = ner_test.get_metrics(y_test, predicted)

precision = correct / c_pred
recall = correct / c_actu
f1 = 2 * precision * recall / (precision + recall)
print(
    'F1 score : {}'.format(f1 * 100))
print(
    'Precision score : {}'.format(precision * 100))
print(
    'Recall score : {}'.format(recall * 100))