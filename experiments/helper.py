# ner = open('../data/ner_tagged_2.txt', 'r')
# tag_dict = {}
# for line in ner:
#     value = line.strip().split('\t')
#     if len(value) == 2:
#         tag = value[1]
#         if tag not in tag_dict:
#             tag_dict[tag] = 0
#         tag_dict[tag] += 1
# print(tag_dict)


import numpy as np

from core.activation import Softmax
from core.model import Model
#from rnnexp.ner import NER

parameter_file = {
    'u': '../parameters/u_rev.npy',
    'v': '../parameters/v_rev.npy',
    'w': '../parameters/w_rev.npy'
}
data_path = {
    'x_train': '../data/reversernn/rev_trainX_notypes.npy',
    'y_train': '../data/reversernn/rev_trainY_notypes.npy',
    'x_test': '../data/reversernn/rev_testX_notypes.npy',
    'y_test': '../data/reversernn/rev_testY_notypes.npy'
}

#
# def get_data():
#     ner_obj = NER(model='wordvect')
#     x_train, y_train = ner_obj.get_data(ner_obj.train_file)
#     x_test, y_test = ner_obj.get_data(ner_obj.test_file)
#     x_dev, y_dev = ner_obj.get_data(ner_obj.dev_file)
#     write_to_file(x_train, y_train, x_test, y_test, x_dev, y_dev)
#     return x_train, y_train, x_test, y_test


def word_vector(z, size):
    binary = '{0:07b}'.format(z)
    return np.array([int(val) for val in binary])


def generator(z, size):
    return z


def write_output(file, data):
    with open(file, 'w') as fw:
        for val in data:
            fw.write(','.join(str(v) for v in val) + '\n')
    fw.close()


def write_to_file(x_train, y_train, x_test, y_test, x_dev, y_dev):
    np.save(data_path['x_train'], x_train)
    np.save(data_path['y_train'], y_train)
    np.save(data_path['x_test'], x_test)
    np.save(data_path['y_test'], y_test)
    np.save(data_path['x_dev'], x_dev)
    np.save(data_path['y_dev'], y_dev)
    # write_output(data_path['x_train'], x_train)
    # write_output(data_path['y_train'], y_train)
    # write_output(data_path['x_test'], x_test)
    # write_output(data_path['y_test'], y_test)


def init_model(u=None, v=None, w=None):
    word_dim = 300
    hidden_dim = 200
    output_dim = 3
    return Model(word_dim, output_dim, Softmax, generator, hidden_dim=hidden_dim, bptt_truncate=0,
                 learning_rate=0.005, optimizer=None,
                 update_type='sequence', u=u, v=v, w=w)


def reverse_data(a, b):
    rev_a = []
    rev_b = []
    for i in range(len(a)):
        rev_a.append(a[i][::-1])
    for i in range(len(a)):
        rev_b.append(b[i][::-1])
    return np.array(rev_a), np.array(rev_b)


def load_data():
    x_train = np.load('../data/gen/trainX_types.npy')
    y_train = np.load('../data/gen/trainY_types.npy')
    x_test = np.load('../data/gen/testX_types.npy')
    y_test = np.load('../data/gen/testY_types.npy')
    x_dev = np.load('../data/gen/devX_types.npy')
    y_dev = np.load('../data/gen/devY_types.npy')
    x_train, y_train = reverse_data(x_train, y_train)
    x_test, y_test = reverse_data(x_test, y_test)
    x_dev, y_dev = reverse_data(x_dev, y_dev)

    write_to_file(x_train, y_train, x_test, y_test, x_dev, y_dev)

#load_data()
