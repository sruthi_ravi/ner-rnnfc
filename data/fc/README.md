###fc_x_test.npy ###
Fully Connected layer test data flattened X values

###fc_y_test.npy ###
Fully Connected layer test data flattened Y values (one hot encoded)

###fc_x_train.npy ###
Fully Connected layer train data flattened X values

###fc_y_train.npy ###
Fully Connected layer train data flattened Y values (one hot encoded)

###y_test_flat.npy ###
Fully Connected layer  test data flattened Y values 

###y_train_flat.npy ###
Fully Connected layer train data flattened Y values 