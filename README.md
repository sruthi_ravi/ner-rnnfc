# README #

### Named Entity Recognition using bi-RNN and separate FC training (using Single RNN networks) ###
This repository has a working model of RNN architecture experimented on named entity recognition. The network is trained using NER tagged twitter dataset. The architecture consists of RNN layers and separate FC layers. 
A sentence represents one train instance and at each time instance, a word is fed as input to the RNN and the tag of the word is predicted as output.
During the test phase, the network predicts appropriate tags for the input sentence.
For sanity check we also configured and ran the keras model and logistic regression for this dataset.

### DataSet ###
* Reference for the dataset - https://github.com/aritter/twitter_nlp/tree/master/data
* The dataset contains set of tagged twitter sentences organized as 'train.txt', 'test.txt', 'dev.txt' (/data/source). 
    - [train/test/dev]_types.txt - ['B-person', 'I-person', 'B-geo-loc', 'I-geo-loc', 'B-company','I-company', Not Named Entity] 
      (only top three classes has been taken for this experiment)
    - [train/test/dev]_notypes.txt- ['B', 'I', Not named entity]
    A new line in the source file defines the start of the new sentence.
* The raw data is preprocessed and word vectors are generated for each word using Google word2vect embedding. Please use this link to download the trained embedding model -https://machinelearningmastery.com/develop-word-embeddings-python-gensim/.
  After the word vectors are generated, the numpy arrays for train/test/dev are saved in /data/gen/ . The words for which the word embeddings are not available as assigned a randomly generated vector.
  The logic to generate the word vectors is in this repo - .They need not be rerun and the preprocessed data saved in /data/gen can directly be used.

### Data Sample ###
* /data/gen/train_types
    - Sample X: Its supposed to rain in The         Bay         on Sunday
    - Sample Y:  O    O       O   O   O B-geo-loc   I-geo-loc    O   O

* /data/gen/train_notypes
    - Sample X: Its supposed to rain in The Bay  on Sunday
    - Sample Y:  O    O       O   O   O  B   I    O   O

* /data/gen/reversernn/rev_train_types
    - Sample X: Sunday on Bay The in rain         to         supposed Its
    - Sample Y:  O    O   I-geo-loc      B-geo-loc   O   O       O   O

* /data/gen/reversernn/rev_train_notypes
    - Sample X: Sunday on Bay The in rain         to         supposed Its
    - Sample Y:  O    O   I    B   O   O       O   O
    
*  /data/fc
    This folder contains intermediate preprocessed files needed for FC training and testing. More details are available in the /data/fc/README.md. 
   
    
### Architecture ###
The bi-RNN is created using two single RNNs in forward and reverse direction. The data is reversed and given as input to the RNN. The output probabilities of the two RNNs are concatenated and passed as 
input to the separate FC layer. The intermediate layers use Sigmoid activation function and final output layer uses Softmax for classification.

### How do I set up? ###

* Model set up
    - /core contains the files for the RNN model architecture.
    - /core/activation - Forward and backward logic for activation functions 
    - /core/gates - RNN gate operations
    - /core/layer - Layer architecture
    - /core/model - RNN Model architecture
* Experiment set up
    /experiments contains the train and test set up files
    /experiments/helper - Helper files for RNN model initialization and configuration.
    - init_model function - defines the configuration such as input, hidden and output dimension and the hyper-parameters for bi-RNN.
    - The helper file also defines the location of the data and parameters weights to save the weights after RNN training. The same paths are used during test time.  
    - /experiments/ner_train - Loads the train data and invoke the model training.
    - /experiments/ner_test - Loads the test data and invoke the model prediction.
    - /experiments/fc_train - The results of single RNNs output is fed as input to FC layer and trained separately
    - /experiments/fc_test - The trained FC model is tested and predictions ate compared with the original classes. 
    - /experiments/fc_helper - This is the helper file to transform the probability result of RNNs (sentences) into flat array of words before giving into experiments/fc_train.
      This file has utilities to make data ready for FC training. This also contains helper function to convert Y classes into one hot encoded vectors for classification.
      
    
* Output and Parameters
    - /output - saves the generated FC predictions probabilities are saved in this directory under /output/fc_predictions.txt
    
    - /parameters - saves the model parameters weight matrices separatly for forward and reverse RNNs in the path specified in the helper files in this directory.
    
    parameter_file = {
    'u': '../parameters/u.npy', #  Forward RNN input to hidden weight
    'v': '../parameters/v.npy', #  Forward RNN hidden to output weight
    'w': '../parameters/w.npy', # #  Forward RNN hidden to hidden weight
    
    'u_rev': '../parameters/u_rev.npy', #  Reverse input to hidden weight
    'v_rev': '../parameters/v_rev.npy', #  Reverse hidden to output weight
    'w_rev': '../parameters/w_rev.npy', # #  Reverse hidden to hidden weight
}
    
* Configurable parameters
    * Epochs
    * Learning rate
    * RNN Input, hidden and output dimension
    * bptt truncate size
    * Optimizer - 'adam' or 'gradient descent'. If nothing is specified default is gradient descent.
    * Parameter update type - 'sequence' or 'window' for end of sequence or window updates.
    * Output activation function 
    * FC layer input, hidden and output dimension
    * Sigmoid + Softmax works better 
    

* How to run
    - To start training of RNN, run experiments/ner_train file.
    - To start testing of RNN, run the /experiments/ner_test file. Check for the correct saved parameters path in /experiments/helper file.
    - To start training of FC layer, run experiments/fc_train.py
    - To start testing of FC layer, run experiments/fc_test.py. Check for the correct saved parameters path in /experiments/fc_helper file.
    - As part of NER prediction the following metrics are computed -
        - F1 score
        - Precision
        - Recall
    
    